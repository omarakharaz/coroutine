#include "generator.h"


namespace oak {

Generator<unsigned> counter6(int to, int from=0)
{
    for(auto i{from}; i<to; i++)
        co_yield i;
}


Generator<std::string> randomstr()
{
    while(true) {
        auto v = rand();
        co_yield "Test-" + std::to_string(v);
    }
}

void main_generator()
{
    auto gen = counter6(5);
    while (gen)
        std::cout << "counter6: " << gen() << std::endl;

    auto rds = randomstr();
    std::cerr << "random = " << rds() << std::endl;
    std::cerr << "random = " << rds() << std::endl;
    std::cerr << "random = " << rds() << std::endl;
}

}

