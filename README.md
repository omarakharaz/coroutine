## Overview ##

POC - coroutine in C++ 20

This project is only a POC that shows an example of using coroutines in C++ 20.

## Prerequisite ##

gcc >= 10.2.0
cmake >= 3.16


## Build ##

From the root of project, create and goto 'build' directory 
```
mkdir build
cd Build
```

Create the cmake configuration files and launch the compilation
```
cmake ..
cmake --build .
```

## Launch ##

```
./src/coroutine
```

## License ##

GNU General Public License version 2 or any later version.
See the file COPYING.
